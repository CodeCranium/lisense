namespace LiSenseWEB
{
    using LiSense;
    using System.Data.Entity;

    public partial class LiSenseDatabaseModel : DbContext
    {
        public LiSenseDatabaseModel() : base("name=LiSenseDatabaseModel")
        {
        }

        public virtual DbSet<LicenseDetail> Licenses { get; set; }
        public virtual DbSet<Owner> Owners { get; set; }
        public virtual DbSet<ServerDetails> ServerDetails { get; set; }
       
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LicenseDetail>().HasOptional(e => e.OwnerDetails).WithMany();
            modelBuilder.Entity<LicenseDetail>().HasOptional(e => e.Server).WithMany();
        }

    }
}
