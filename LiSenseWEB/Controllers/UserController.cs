﻿using LiSense;
using System;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.Mvc;

namespace LiSenseWEB.Controllers
{
    public class UserController : Controller
    {
        public ActionResult RegisterLicense()
        {
            ViewBag.Title = "Register License";
            return View(new LiSense.LicenseDetail());
        }

        [HttpPost]
        public ActionResult RegisterLicense(Guid licenseKey)
        {

            using (var db = new LiSenseDatabaseModel())
            {
                // Search for matching License Key (proceed if found)
                // Check if 'IsRegistered'
                // If 'IsRegistered' then check if 'NumberOfLicensesClaimed' => 'NumberOfLicensesAllowed'
                //      If ('NumberOfLicensesClaimed' => 'NumberOfLicensesAllowed') then FAIL (User is not able to claim anymore licenses)
                //      If NOT ('NumberOfLicensesClaimed' => 'NumberOfLicensesAllowed') claim a license, and +1 to the 'NumberOfLicensesClaimed'
                // If NOT 'IsRegisterd' then goto Registration page to allow user to input details about theirself for the Owner table, and then return to here

                var licenseKeyFound = db.Licenses.Where(x => x.LicenseKey == licenseKey).Include(x => x.OwnerDetails).Include(x => x.Server).FirstOrDefault();
                if (licenseKeyFound != null)
                {
                    if (!CheckIfLicenseIsRegisteredToUser(licenseKeyFound))
                    {
                        TempData["OwnerId"] = licenseKeyFound.OwnerDetails.OwnerId;
                        return RedirectToAction("RegisterUser");
                    }
                    // User Details are in the DB
                    if (licenseKeyFound.NumberOfLicensesClaimed >= licenseKeyFound.NumberOfLicensesAllowed)
                    {
                        // User has claimed MAX number of licenses allocated to them.  [FAIL]
                        ViewBag.Message = "No valid license(s) available to claim.  Please contact support if you feel that you have reached this message in error.";
                        return View();
                    }
                    else
                    {
                        // Add +1 to NumberOfLicensesClaimed and grant the User a License
                    }
                }
            }
            return View(new LiSense.LicenseDetail());
        }

        [HttpPost]
        public void Validate(Models.LicenseDataModel licenseDetail)
        {
            if (ModelState.IsValid)
            {

            }
        }

        private bool CheckIfLicenseIsRegisteredToUser(LiSense.LicenseDetail license)
        {
            if (!license.OwnerDetails.IsRegistered)
            {
                // Register User (Get their details, and populate their details for the Owner Table (then send them back to claim license)
                return false;
            }

            return true;
        }

        public ActionResult RegisterUser()
        {
            TempData["OwnerId"] = TempData["OwnerId"] != null ? TempData["OwnerId"] : null;  // Carry over the OwnerId to the next page
            ViewBag.ReturnUrl = "RegisterLicense";
            
            return View(new LiSense.Owner());
        }

        [HttpPost]
        public ActionResult RegisterUser(LiSense.Owner ownerRecord)
        {
            if (ModelState.IsValid)
            {
                using (var db = new LiSenseDatabaseModel())
                {

                    //db.Owners.Attach(ownerRecord);  //.Where(x=>x.OwnerId == ownerRecord.OwnerId).FirstOrDefault();
                    var newUserDetails = db.Owners.Find(TempData["OwnerId"]);
                    newUserDetails.IsRegistered = true;
                    newUserDetails.OwnerFirstName = ownerRecord.OwnerFirstName;
                    newUserDetails.OwnerLastName = ownerRecord.OwnerLastName;
                    newUserDetails.OwnerAddress = ownerRecord.OwnerAddress;
                    newUserDetails.City = ownerRecord.City;
                    newUserDetails.State = ownerRecord.State;
                    newUserDetails.ZipCode = ownerRecord.ZipCode;

                    db.SaveChanges();
                }

                return RedirectToAction("RegisterLicense");
            }

            ViewBag.Message = "Invalid Details provided.";
            return View();

        }


        //[HttpPost]
        //public WebReply AddLicenseDetails<T>(T objectToUpdate)
        //{

        //    using (LiSenseDatabaseModel db = new LiSenseDatabaseModel())
        //    {
        //        if (objectToUpdate.GetType().ToString().Contains("ServerDetail"))
        //        {
        //            // TODO:  Add Generics methods to update (if needed)
        //        }
        //    }

        //    new NotImplementedException();
        //    return null;
        //}


        [HttpPost]
        public WebReply AddLicenseDetails(LicenseDetail licenses)
        {
            WebReply wr = new WebReply();
            try
            {
                using (LiSenseDatabaseModel db = new LiSenseDatabaseModel())
                {
                    db.Configuration.ValidateOnSaveEnabled = false;
                    var dbLicense = db.Licenses.Where(x => x.LicenseKey == licenses.LicenseKey).FirstOrDefault();
                    if (dbLicense.Server != null)
                    {
                        // Only update DB values that are null/empty

                        //var licenseServer = db.ServerDetails.FirstOrDefault(x => x.ServerId == dbLicense.Server.ServerId);
                        //licenseServer.Server.HostName = licenses.Server.Server.HostName;
                        //licenseServer.MotherBoard.ManufacturerName = licenses.Server.MotherBoard.ManufacturerName;
                        //licenseServer.MotherBoard.SerialNumber = licenses.Server.MotherBoard.SerialNumber;

                                             

                        //dbLicense.Server = new ServerDetails()
                        //{
                        //    Server = licenses.Server.Server,
                        //    MotherBoard = licenses.Server.MotherBoard
                        //    //Server = new ServerDetails.ServerDetail()
                        //    //{
                        //    //    HostName = licenses.Server.Server.HostName,
                        //    //},
                        //    //MotherBoard = new ServerDetails.MotherBoardDetail()
                        //    //{
                        //    //    ManufacturerName = licenses.Server.MotherBoard.ManufacturerName,
                        //    //    SerialNumber = licenses.Server.MotherBoard.SerialNumber
                        //    //}
                        //};

                        db.SaveChanges();
                    }
                }

                return null;

            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        wr.Errors.Add(new ErrorEvents() { ErrorMessage = String.Format("Error: {0} - Property: {1}", validationError.ErrorMessage, validationError.PropertyName) });
                    }
                }
                return wr;
            }
            catch (Exception ex)
            {
                wr.Errors.Add(new ErrorEvents(){ ErrorMessage = ex.Message});
                return wr;
            }
        }
    }
}
