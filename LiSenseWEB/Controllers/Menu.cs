﻿using System.Collections.Generic;

namespace LiSenseWEB.Controllers
{
    public class Menu
    {
        public IEnumerable<MenuItem> menuItems { get; set; }
        public string MenuName { get; set; }

        public int OrderLevel { get; set; }
        public Menu()
        {
            menuItems = new List<MenuItem>();
        }
    }
}
