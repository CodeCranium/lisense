﻿using LiSense;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace LiSenseWEB.Controllers
{
    public class AdminController : Controller
    {
        LiSenseDatabaseModel db = new LiSenseDatabaseModel();
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult AddNewLicense()
        {
            ViewBag.Title = "Add New License";

            var lKey = Guid.NewGuid();

            return View(new LiSense.LicenseDetail()
            {
                LicenseKey = lKey,
            });
        }

        [HttpPost]
        public ActionResult AddNewLicense(LicenseDetail license)
        {
            if (ModelState.IsValid)  // Checks that the properties that are marked with the [Required] attribute and validation is correct, if not it will redirect back to View and show the error items by using the @Html.ValidationMessageFor in the View
            {
                db.Licenses.Add(license);
                db.SaveChanges();
            }
            
            return View();
        }

        public ActionResult RetrieveLicenseDetail()
        {
            ViewBag.Title = "Search for License";
            return View(new LicenseDetail());
        }

        [HttpPost]
        public ActionResult RetrieveLicenseDetail(LicenseDetail license)
        {
            LicenseDetail foundLicense = new LicenseDetail();
            if (ModelState.IsValid)
            {
                if(license.OwnerDetails.OwnerFirstName != null)
                {
                    foundLicense = db.Licenses.Where(x => x.OwnerDetails.OwnerFirstName.Contains(license.OwnerDetails.OwnerFirstName)).Include(o=>o.OwnerDetails).FirstOrDefault();
                }
                else if (license.OwnerDetails.OwnerLastName != null)
                {
                    foundLicense = db.Licenses.Where(x => x.OwnerDetails.OwnerLastName.Contains(license.OwnerDetails.OwnerLastName)).Include(o => o.OwnerDetails).FirstOrDefault();
                }

                else if (license.LicenseKey != null)
                {
                    foundLicense = db.Licenses.Where(x => x.LicenseKey == (license.LicenseKey)).Include(o => o.OwnerDetails).FirstOrDefault();
                }
                                
                return View(foundLicense);
            }
            ViewBag.Message = "Incorrect format in item parameters specified.";
            return View(license);
        }


        public static List<string> GetEnumValues(Type enumType)
        {
            List<string> itemList = new List<string>();

            var listOfValues = Enum.GetValues(enumType);
            foreach (var value in listOfValues)
            {
                itemList.Add(value.ToString());
            }

            return itemList;
        }


        
    }
}
