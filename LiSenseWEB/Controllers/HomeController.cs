﻿using LiSense;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Mvc;


namespace LiSenseWEB.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            return View();
        }



        public static IEnumerable<Menu> GetMenuItems()
        {
            List<Menu> menu = new List<Menu>();
            
            // Admin Menu
            menu.Add(new Menu()
            {
                MenuName = "ADMIN",
                menuItems = new List<MenuItem>(){
                    new MenuItem(){
                                Name = "Add New License",
                                ActionName = "AddNewLicense",
                                ControllerName = "Admin"}, 
                    new MenuItem(){
                                Name = "Search for License",
                                ActionName = "RetrieveLicenseDetail",
                                ControllerName = "Admin"}, 
                },
                OrderLevel = 99
            });


            // User Menu
            menu.Add(new Menu()
            {
                MenuName = "User",
                menuItems = new List<MenuItem>(){
                    new MenuItem(){
                                Name = "Register Account",
                                ActionName = "RegisterUser",
                                ControllerName = "User"}, 
                    new MenuItem(){
                                Name = "Register License",
                                ActionName = "RetrieveLicenseDetail",
                                ControllerName = "User"}, 
                },
                OrderLevel = 1
            });





            return menu;
        }
    }


}
