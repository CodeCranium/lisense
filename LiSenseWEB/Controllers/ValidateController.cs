﻿using LiSense;
using System.Linq;
using System.Web.Http;

namespace LiSenseWEB.Controllers
{
    public class ValidateController : ApiController
    {
        // POST api/validate
        [HttpPost]
        public WebReply Post([FromBody]LicenseDetail value)
        {
            if (ModelState.IsValid)
            {
                using (var lisenseDb = new LiSenseDatabaseModel())
                {
                    var licenseMatch = lisenseDb.Licenses.FirstOrDefault(x => x.LicenseKey == value.LicenseKey);
                    if (licenseMatch != null)
                    {
                        // Found Matching License (check the details to make sure they match the product the user is trying to use).
                        if (validateLicenseDetails((LiSense.LicenseDetail)value, licenseMatch))
                        {
                            return new WebReply()
                            {
                                LicensePassedAuth = true,
                                ResponesLicenseDetails = licenseMatch
                            };
                        }
                    }
                }
            }

            return new WebReply()
            {
                LicensePassedAuth = false
            };
        }

        public string Get()
        {
            return "GET Request is not supported.";
        }

        private bool validateLicenseDetails(LiSense.LicenseDetail sentLicenseDetails, LiSense.LicenseDetail databaseLicenseDetails)
        {
            // Validate - ProductName, LicenseLevel, and ServerDetails (Hostname, Motherboard serial)
            return sentLicenseDetails.ProductName.Equals(databaseLicenseDetails.ProductName) &&
                sentLicenseDetails.LicenseLevel.Equals(databaseLicenseDetails.LicenseLevel) &&
                sentLicenseDetails.Server.Server.HostName.Equals(databaseLicenseDetails.Server.Server.HostName);
        }



        // // GET api/validate
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // // GET api/validate/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

    }
}