﻿using LiSense;
using System;

namespace LiSenseWEB.Models
{
    public class LicenseDataModel
    {
        public Guid LicenseKey { get; set; }

        public string ProductName { get; set; }
        
        public LicenseSubscription.LicenseType LicenseLevel { get; set; }
        
        public int NumberOfLicensesAllowed { get; set; }
        public int NumberOfLicensesClaimed { get; set; }

        public Owner OwnerDetails { get; set; }
        public ServerDetails Server { get; set; }

        public DateTime? LicenseValidUntilCurrent { get; set; }
               
     
      

    }
}