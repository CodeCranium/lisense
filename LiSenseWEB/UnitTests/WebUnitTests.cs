﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LiSenseWEB.UnitTests
{
    [TestClass]
    public class WebUnitTests
    {
        [TestMethod]
        public void ShouldCreateDatabase()
        {
            using (var x = new LiSenseDatabaseModel())
            {
                x.Licenses.Add(new LiSense.LicenseDetail(){
                     LicenseLevel = LiSense.LicenseSubscription.LicenseType.Full,
                     ProductName = "App123",
                       //LicenseValidUntilCurrent = DateTime.Now,
                     Server = new LiSense.ServerDetails()
                     {
                         Server = new LiSense.ServerDetails.ServerDetail()
                         {
                              HostName = "Test123",
                         },
                         MotherBoard = new LiSense.ServerDetails.MotherBoardDetail()
                         {
                              ManufacturerName = "TEST",
                               SerialNumber = "Test123 DEBUG"
                         }
                     },


                });
                x.SaveChanges();
            }

        }
    }
}