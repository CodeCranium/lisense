namespace LiSenseWEB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Licenses",
                c => new
                    {
                        LicenseKey = c.Guid(nullable: false, identity: true),
                        ProductName = c.String(),
                        LicenseLevel = c.Int(nullable: false),
                        NumberOfLicensesAllowed = c.Int(nullable: false),
                        NumberOfLicensesClaimed = c.Int(nullable: false),
                        LicenseValidUntilCurrent = c.DateTime(),
                        OwnerDetails_OwnerId = c.Int(),
                        Server_ServerId = c.Guid(),
                    })
                .PrimaryKey(t => t.LicenseKey)
                .ForeignKey("dbo.Owners", t => t.OwnerDetails_OwnerId)
                .ForeignKey("dbo.ServerDetails", t => t.Server_ServerId)
                .Index(t => t.OwnerDetails_OwnerId)
                .Index(t => t.Server_ServerId);
            
            CreateTable(
                "dbo.Owners",
                c => new
                    {
                        OwnerId = c.Int(nullable: false, identity: true),
                        OwnerLastName = c.String(),
                        OwnerFirstName = c.String(),
                        OwnerAddress = c.String(),
                        City = c.String(),
                        State = c.String(),
                        ZipCode = c.Int(nullable: false),
                        IsRegistered = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.OwnerId);
            
            CreateTable(
                "dbo.ServerDetails",
                c => new
                    {
                        ServerId = c.Guid(nullable: false, identity: true),
                        MotherBoard_SerialNumber = c.String(),
                        MotherBoard_ManufacturerName = c.String(),
                        Server_HostName = c.String(),
                    })
                .PrimaryKey(t => t.ServerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Licenses", "Server_ServerId", "dbo.ServerDetails");
            DropForeignKey("dbo.Licenses", "OwnerDetails_OwnerId", "dbo.Owners");
            DropIndex("dbo.Licenses", new[] { "Server_ServerId" });
            DropIndex("dbo.Licenses", new[] { "OwnerDetails_OwnerId" });
            DropTable("dbo.ServerDetails");
            DropTable("dbo.Owners");
            DropTable("dbo.Licenses");
        }
    }
}
