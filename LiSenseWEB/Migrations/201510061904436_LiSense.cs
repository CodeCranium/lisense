namespace LiSenseWEB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LiSense : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Owners", "ZipCode", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Owners", "ZipCode", c => c.Int(nullable: false));
        }
    }
}
