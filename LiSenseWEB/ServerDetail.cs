namespace LiSenseWEB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ServerDetail
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ServerDetail()
        {
            Licenses = new HashSet<License>();
        }

        [Key]
        public Guid ServerId { get; set; }

        public string MotherBoard_SerialNumber { get; set; }

        public string MotherBoard_ManufacturerName { get; set; }

        public string Server_HostName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<License> Licenses { get; set; }
    }
}
