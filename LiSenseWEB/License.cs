namespace LiSenseWEB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class License
    {
        [Key]
        public Guid LicenseKey { get; set; }

        public string ProductName { get; set; }

        public int LicenseLevel { get; set; }

        public DateTime LicenseValidUntilCurrent { get; set; }

        public Guid? Server_ServerId { get; set; }

        public virtual ServerDetail ServerDetail { get; set; }
    }
}
