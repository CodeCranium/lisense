﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LiSense
{
    //[ComplexType]  // This attribute allows Entity Framework to Map the Foreign key needed to match values from the Licenses Database
    public class Owner
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]  // Key and Auto Identified id
        [ScaffoldColumn(false)]  // Keeps it from using it in @Html.EditorForModel() in Razor
        public int OwnerId { get; set; }

        [DisplayName("Last Name")]
        public string OwnerLastName { get; set; }

        [DisplayName("First Name")]
        public string OwnerFirstName { get; set; }

        [DisplayName("Address")]
        public string OwnerAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        
        [Range(10000, 99999, ErrorMessage="Zip Code must be 5 digits")]
        public int ZipCode { get; set; }

        [ScaffoldColumn(false)]
        public bool IsRegistered { get; set; }
    }
}
