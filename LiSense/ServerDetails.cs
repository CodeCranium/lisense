﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Management;

namespace LiSense
{
    //[ComplexType]  // This attribute allows Entity Framework to Map the Foreign key needed to match values from the Licenses Database
    [Table("ServerDetails")]
    public class ServerDetails
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]  // Key and Auto Identified id
        public Guid ServerId { get; set; }
        public MotherBoardDetail MotherBoard { get; set; }
        public ServerDetail Server { get; set; }

        public ServerDetails()
        {
            MotherBoard = new MotherBoardDetail();
            Server = new ServerDetail();
        }

        public static ServerDetails GetServerDetails()
        {
            //#if(DEBUG)
            //{
            //    var serverDetailsDEBUG = new ServerDetails() { };
            //    serverDetailsDEBUG.MotherBoard.SerialNumber = "123Test_DEBUG";
            //    serverDetailsDEBUG.MotherBoard.ManufacturerName = "123Testing";
            //    serverDetailsDEBUG.Server.HostName = Environment.MachineName;

            //    return serverDetailsDEBUG;
            //}
            //#endif

            ManagementObjectSearcher motherboardDetails = new ManagementObjectSearcher("SELECT * FROM Win32_BaseBoard");
            ManagementObjectCollection moc = motherboardDetails.Get();

            var serverDetails = new ServerDetails() { };

            foreach (ManagementObject mo in moc)
            {
                serverDetails.MotherBoard.SerialNumber = mo["SerialNumber"].ToString();
                serverDetails.MotherBoard.ManufacturerName = mo["Product"].ToString();
                serverDetails.Server.HostName = Environment.MachineName;
            }

            //var jsonResponse = JsonConvert.SerializeObject(serverDetails);


            return serverDetails;

        }

        
        public class ServerDetail
        {
            public string HostName { get; set; }
            public TimeZone ServerTimeZone
            {
                get
                {
                    return TimeZone.CurrentTimeZone;
                }
            }
        }

        public class MotherBoardDetail
        {
            public string SerialNumber { get; set; }
            public string ManufacturerName { get; set; }

        }

    }
}
