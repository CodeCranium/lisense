﻿using System.Collections.Generic;
namespace LiSense
{
    public class WebReply
    {
        public bool LicensePassedAuth { get; set; }
        public LicenseDetail ResponesLicenseDetails { get; set; }

        public List<ErrorEvents> Errors { get; set; }

        public WebReply()
        {
            Errors = new List<ErrorEvents>();

        }
    }

}
