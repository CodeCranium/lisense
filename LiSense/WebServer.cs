﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace LiSense
{
    public class WebServer 
    {
        private License _license { get; set; }
        public WebServer(License License)
        {
            _license = License;
        }

        public WebReply GetWebRequest(WebRequest webRequest)
        {
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse())
                {
                    Stream receiveStream = response.GetResponseStream();  // Get the stream associated with the response.
                    StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);  // Pipes the stream to a higher level stream reader with the required encoding format.

                    var jsonConvertToWebResponse = JsonConvert.DeserializeObject<WebReply>(readStream.ReadToEnd());
                    return jsonConvertToWebResponse;
                }
            }
            catch (Exception webRequestException)
            {
                //string pageContent = new StreamReader(response.Response.GetResponseStream()).ReadToEnd().ToString();
   
                _license.Errors.Add(new ErrorEvents
                {
                    ErrorCode = ErrorEvents.Code.WebRequest,
                    ErrorMessage = webRequestException.Message
                });
                return null;
                throw webRequestException;
            }
        }

        public Uri GetRemoteEndpoint()
        {
            //Requires setting to be placed in the core application's [app.config]
            // in the <appSettings> node in the <configuration> node with the following:  <add key="LiSense.endPointUrl" value="http://123.1.2.3/"/> Values for the endpoints will be populated in the application (just need core base endpoint)
            try
            {
                var clientsFilePath = new Uri(ConfigProperty.GetValue("LiSense.endPointUrl"));
                return clientsFilePath;
            }
            catch (Exception configFileException)
            {
                _license.Errors.Add(new ErrorEvents
                {
                    ErrorCode = ErrorEvents.Code.Config,
                    ErrorMessage = configFileException.Message
                });
                throw;
            }
        }

        internal WebReply ValidateAgainstRemoteAuthServer()
        {
            return PostToWebServer<LicenseDetail>(@"user/Validate", _license.LicenseDetails);
        }
        

        public WebReply PostToWebServer<T>(string apiEndPointName, T objectToSend)
        {
            try
            {
                var authEndPoint = new Uri(GetRemoteEndpoint(), apiEndPointName);
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(authEndPoint);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var jsonPostData = JsonConvert.SerializeObject(objectToSend);
                    //var data = Encoding.ASCII.GetBytes(jsonPostData);
                    streamWriter.Write(jsonPostData);
                    //streamWriter.Flush();
                    //streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                }

                WebReply webResponse = GetWebRequest(httpWebRequest);
                
                if (webResponse != null)
                {
                    return webResponse;
                }

                return null;
                

                //// -------------------------------------------------------
                //var authEndPoint = new Uri(GetRemoteEndpoint(), apiEndPointName);
                //var webRequest = HttpWebRequest.Create(authEndPoint);

                //var jsonPostData = JsonConvert.SerializeObject(objectToSend);
                //var data = Encoding.ASCII.GetBytes(jsonPostData);

                //webRequest.Method = "POST";
                //webRequest.ContentType = @"application/json";

                //webRequest.ContentLength = data.Length;

                //// TODO:  Add a try/catch for fail-over option if the computer can't get to the network address provided. (Only allow x amount of times before it has to check in with server)
                //using (var stream = webRequest.GetRequestStream())
                //{
                //    stream.Write(data, 0, data.Length);
                //}

                //WebReply webResponse = GetWebRequest(webRequest);

                //if (webResponse != null)
                //{
                //    return webResponse;
                //}

                //return null;
            }
            catch (Exception webRequestException)
            {
                _license.Errors.Add(new ErrorEvents
                {
                    ErrorCode = ErrorEvents.Code.WebRequest,
                    ErrorMessage = webRequestException.Message
                });
                return null;
                throw webRequestException;
            }
        }




    }
}
