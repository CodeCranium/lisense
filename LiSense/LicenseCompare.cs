﻿

using System;
using System.Reflection;
using System.Runtime.Serialization;


namespace LiSense
{
    [DataContract]
    public class LicenseCompare
    {
        public LicenseDetail OldLicense { get; set; }
        public LicenseDetail NewLicense { get; set; }

        public LicenseCompare(LicenseDetail oldLicense, LicenseDetail newLicense)
        {
            OldLicense = oldLicense;
            NewLicense = newLicense;
        }

        internal LicenseDetail Merge()
        {
            //TODO:  Do an actual merge on the old and new details (right now it is just sending back the new license)

            NewLicense.OwnerDetails = OldLicense.OwnerDetails;
            NewLicense.LicenseKey = OldLicense.LicenseKey;  // Send the LicenseKey to the New licesnse, so I can reference it in the merged on the AddLicenseDetails endpoint.
            return NewLicense;
        }
    }

    public static class CompareExchanger
    {
        public enum DetailLevel
        {
            MotherBoard,
            Server
        }

       
        public static object GetObjProperty(object obj, string property)
        {
            Type t = obj.GetType();
            PropertyInfo p = t.GetProperty(property);
            var val = p.GetValue(obj, null);
            return val;
        }

    }
}
