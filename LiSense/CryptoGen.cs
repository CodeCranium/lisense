﻿using System;
using System.ComponentModel;
using System.Security.Cryptography;
using System.Text;

namespace LiSense
{
    internal class CryptoGen
    {
        [EditorBrowsable(EditorBrowsableState.Never)]  // Used to hide the property value from external view in debugging.
        private const string Key = "abc123";

        [EditorBrowsable(EditorBrowsableState.Never)]
        private readonly static byte[] IVector = new byte[8] { 81, 7, 17, 30, 0, 10, 114, 27 };

        public enum CryptoType
        {
            Encrypt,
            Decrypt
        }

        //TODO:  Abstract the 2 methods into 1 single method

        internal static string Encrypt(string toEncrypt)
        {
            byte[] buffer = Encoding.ASCII.GetBytes(toEncrypt);
            TripleDESCryptoServiceProvider tripleDes = new TripleDESCryptoServiceProvider();
            MD5CryptoServiceProvider MD5 = new MD5CryptoServiceProvider();
            tripleDes.Key = MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(Key));
            tripleDes.IV = IVector;
            ICryptoTransform ITransform = tripleDes.CreateEncryptor();
            return Convert.ToBase64String(ITransform.TransformFinalBlock(buffer, 0, buffer.Length));
        }

        internal static string Decrypt(string toDecrypt)
        {
            byte[] buffer = Convert.FromBase64String(toDecrypt);
            TripleDESCryptoServiceProvider tripleDes = new TripleDESCryptoServiceProvider();
            MD5CryptoServiceProvider MD5 = new MD5CryptoServiceProvider();
            tripleDes.Key = MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(Key));
            tripleDes.IV = IVector;
            ICryptoTransform ITransform = tripleDes.CreateDecryptor();
            return Encoding.ASCII.GetString(ITransform.TransformFinalBlock(buffer, 0, buffer.Length));
        }
    }
}
