﻿
namespace LiSense
{
    public class ErrorEvents
    {
        public Code ErrorCode { get; set; }
        public string ErrorMessage { get; set; }

        public enum Code
        {
            Config,
            WebRequest,
            Validation,
            InvalidUniqueId,
            LicenseFile,
        }
    }
}
