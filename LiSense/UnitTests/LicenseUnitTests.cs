﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Linq;

namespace LiSense.UnitTests
{
    [TestClass]
    public class LicenseUnitTests
    {
        
        [TestMethod]
        public void ShouldValidateLicense()
        {
            License xLicense = new License();

            var validateLicense = xLicense.Validate();
            if (validateLicense && !xLicense.Errors.Any())
            {
                Debug.WriteLine("You are now in the application...");
            }

            Debug.WriteLine("Exiting application, License does not match...");
            Environment.Exit(1);

            Assert.IsFalse(xLicense.Errors.Any(), "Make sure that there are currently NO errors");
        }

        [TestMethod]
        public void ShouldCreateLicenseFile()
        {
            License x = new License()
            {
                LicenseDetails = new LicenseDetail()
                {
                     LicenseKey = new Guid(),
                      LicenseLevel = LicenseSubscription.LicenseType.Full,
                       LicenseValidUntilCurrent = new DateTime(2015, 1, 31),
                        NumberOfLicensesAllowed = 1,
                        NumberOfLicensesClaimed = 0,
                         OwnerDetails = new Owner() {
                             OwnerFirstName = "Test",
                             OwnerLastName = "DEBUG",
                             OwnerAddress = "123 Test St",
                             City = "Nowhere Ville",
                               State = "TX",
                               ZipCode = 12345,
                                IsRegistered = true
                         },
                          ProductName = "TestApp.exe",
                          Server = new ServerDetails() {
                               MotherBoard = new ServerDetails.MotherBoardDetail() {
                                    ManufacturerName = "DELL Test",
                                    SerialNumber = "1234567890"
                               },
                               Server = new ServerDetails.ServerDetail() {
                                    HostName = "TestSvr123",

                               }
                          }
                }                
            };
            
            x.Create("License.lic");

        }





        //[TestMethod]
        //public void ShouldCompareLicenses()
        //{
        //    License xOLD = new License()
        //    {
        //        LicenseDetails = new LicenseDetail()
        //        {
        //            LicenseKey = new Guid(),
        //            LicenseLevel = LicenseSubscription.LicenseType.Full,
        //            LicenseValidUntilCurrent = new DateTime(2015, 1, 31),
        //            NumberOfLicensesAllowed = 1,
        //            NumberOfLicensesClaimed = 0,
        //            OwnerDetails = new Owner()
        //            {
        //                OwnerFirstName = "Test",
        //                OwnerLastName = "DEBUG",
        //                OwnerAddress = "123 Test St",
        //                City = "Nowhere Ville",
        //                State = "TX",
        //                ZipCode = 12345,
        //                IsRegistered = true
        //            },
        //            ProductName = "TestApp.exe",
        //            Server = new ServerDetails()
        //            {
        //                MotherBoard = new ServerDetails.MotherBoardDetail()
        //                {
        //                    ManufacturerName = "DELL Test",
        //                    SerialNumber = "1234567890"
        //                },
        //                Server = new ServerDetails.ServerDetail()
        //                {
        //                    HostName = "TestSvr123_OLD",

        //                }
        //            }
        //        }
        //    };


        //    License xNEW = new License()
        //    {
        //        LicenseDetails = new LicenseDetail()
        //        {
        //            LicenseKey = new Guid(),
        //            LicenseLevel = LicenseSubscription.LicenseType.Full,
        //            LicenseValidUntilCurrent = new DateTime(2015, 1, 31),
        //            NumberOfLicensesAllowed = 1,
        //            NumberOfLicensesClaimed = 0,
        //            OwnerDetails = new Owner()
        //            {
        //                OwnerFirstName = "Test",
        //                OwnerLastName = "DEBUG",
        //                OwnerAddress = "123 Test St",
        //                City = "Nowhere Ville",
        //                State = "TX",
        //                ZipCode = 12345,
        //                IsRegistered = true
        //            },
        //            ProductName = "TestApp.exe",
        //            Server = new ServerDetails()
        //            {
        //                MotherBoard = new ServerDetails.MotherBoardDetail()
        //                {
        //                    ManufacturerName = "DELL Test",
        //                    SerialNumber = "1234567890"
        //                },
        //                Server = new ServerDetails.ServerDetail()
        //                {
        //                    HostName = "BRAND_NEW_HOSTNAME",

        //                }
        //            }
        //        }
        //    };

        //    LicenseCompare xCompare = new LicenseCompare(xOLD.LicenseDetails, xNEW.LicenseDetails);
        //    var xHostname = xCompare.CheckToChange(CompareExchanger.DetailLevel.Server, "HostName");

        //}


    }
}
