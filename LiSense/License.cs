using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace LiSense
{
    /// <summary>
    /// Note:  Requires setting to be placed in the core application's [app.config] for the <appSettings> node with the following: 
    /// <add key="LiSense.endPointUrl" value="http://LiSenseServerIP"/>  // The LiSense server that the License authorization points to.
    /// <add key="LiSense.validUntilDate" value="1/1/2016"/>  // The initial date that the local .lic file is valid till until needing to check remote server for validation.
    /// </summary>
    public class License
    {
        public LicenseDetail LicenseDetails { get; set; }
        public List<ErrorEvents> Errors { get; set; }

        public License(LicenseDetail license)
        {
            Errors = new List<ErrorEvents>();
            LicenseDetails = license;
        }

        [Obsolete]
        public License()
        {
            Errors = new List<ErrorEvents>();
            LicenseDetails = new LicenseDetail();
        }


        public License(string licenseFile)
        {
            Errors = new List<ErrorEvents>();
            try
            {
                if (File.Exists(licenseFile))
                {
                    var licenseDetail = License.Load(licenseFile);
                    LicenseDetails = licenseDetail;
                }
                else
                {
                    throw new FileNotFoundException(String.Format("License file specified: {0} not found!", licenseFile));
                }  
            }
            catch (Exception fileReadException)
            {
                Errors.Add(new ErrorEvents()
                {
                    ErrorCode = ErrorEvents.Code.LicenseFile,
                    ErrorMessage = fileReadException.Message
                });
                throw;
            }
        }

        /// <summary>
        /// Process to validate the validity of the product against the LiSense server.
        /// After invoking the process, you can check for any resulting errors in the 'Errors' property of the object (if returns False).
        /// </summary>
        /// <returns> [Boolean] True-(Passed) / False-(Failed) the authorization process for licensing. </returns>
        public bool Validate()
        {
            try
            {
                DateTime licenseValidUntil = LicenseDetails.LicenseValidUntilCurrent ?? default(DateTime);

                if ((DateTime.Compare(licenseValidUntil, DateTime.Now) <= 0) && (EnsureLicenseDetailsValid()))
                {
                    // License is due for checking.
                    var ws = new WebServer(this);

                    var returnedWebResponse = ws.ValidateAgainstRemoteAuthServer();
                    if (returnedWebResponse.LicensePassedAuth)
                    {
                        Task<bool> checkIfValidLicense = new Task<bool>(() => ValidateLocalServerToLicenseDetails(returnedWebResponse.ResponesLicenseDetails));
                        checkIfValidLicense.Start();
                        checkIfValidLicense.Wait();
                        return checkIfValidLicense.Result;


                        //var validatedLicense = ValidateLocalServerToLicenseDetails(returnedWebResponse.ResponesLicenseDetails);
                        //return validatedLicense;
                    }
                    return false;
                }

                // License is valid until further date in time.
                return true;
            }
            catch (Exception validationException)
            {
                Errors.Add(new ErrorEvents
                {
                    ErrorCode = ErrorEvents.Code.Validation,
                    ErrorMessage = validationException.Message
                });
                return false;
            }
        }

        private bool EnsureLicenseDetailsValid()
        {
            // Ensure that the License was constructed with parameters and not just sending Blank/NULL license.
            if ((LicenseDetails != null) & (CheckLicenseIsInitalized()))
            {
                return true;
            }
            return false;
        }

        private bool CheckLicenseIsInitalized()
        {
            if (LicenseDetails.Server.ServerId == Guid.Empty)
            {
                // Details for a registered server are not currently associated with the License.  
                // - No need to route user elsewhere, just send details to DB Server to update the details, and update the details in the local .lic file after successfully updated on DB
               
                // Send the server details to the DB Server with details about the licenee's server
                
                var localServerDetails = ServerDetails.GetServerDetails();

                WebServer dbUpdate = new WebServer(this);
                var newLicense = new LicenseDetail(){
                    Server = new ServerDetails()
                    {
                        MotherBoard = new ServerDetails.MotherBoardDetail()
                        {
                             ManufacturerName = localServerDetails.MotherBoard.ManufacturerName,
                             SerialNumber = localServerDetails.MotherBoard.SerialNumber
                        },
                        Server = new ServerDetails.ServerDetail()
                        {
                             HostName = localServerDetails.Server.HostName,

                        }
                    },
                    
                };

                LicenseCompare mergedLicenseDetail = new LicenseCompare(this.LicenseDetails, newLicense);
                var responseUpdateServerDetails = dbUpdate.PostToWebServer<LicenseDetail>(@"user/AddLicenseDetails", mergedLicenseDetail.Merge());

                // TODO:  After sending/updating the details about the server to the DB, then Save the server details to the .lic file (so they match)


            }

            return true;
        }


        private bool ValidateLocalServerToLicenseDetails(LicenseDetail webResponseLicense)
        {
            if (LicenseDetails.Equals(webResponseLicense))  // TODO: Do granular validation instead of object equality comparison, currently it is just comparing Object to Object
            {
                // TODO:  Add x amount of days to the amount of time until next license check will be needed to be checked.
                // Value to update is found in the (config.AppSettings.Settings["LiSense.endPointUrl"].Value)

                AddTimeToValidUntilDate();
                return true;
            }

            Errors.Add(new ErrorEvents { ErrorCode = ErrorEvents.Code.InvalidUniqueId, ErrorMessage = "Invalid License provided for authorization, please contact support." });
            return false;
        }

        private void AddTimeToValidUntilDate()
        {
            ConfigProperty.SaveValue("LiSense.validUntilDate", DateTime.Now.AddDays(30).ToShortDateString());
        }


        public void Create(string licenseFileName)
        {
            var licenseData = Newtonsoft.Json.JsonConvert.SerializeObject(this.LicenseDetails);
            var encryptedLicenseData = CryptoGen.Encrypt(licenseData);
            
            using (var licenseFile = new StreamWriter(licenseFileName, false))
            {
                licenseFile.Write(encryptedLicenseData);
            }
        }

                
        public static LicenseDetail Load(string licenseFileName)
        {
            LicenseDetail licenseData;

            using (var licenseFile = new StreamReader(licenseFileName))
            {
                var decryptedLicenseData = CryptoGen.Decrypt(licenseFile.ReadToEnd());
                licenseData = Newtonsoft.Json.JsonConvert.DeserializeObject<LicenseDetail>(decryptedLicenseData);
            }

            return licenseData;
        }
    }
}
