﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LiSense
{
    [Table("Licenses")]
    public class LicenseDetail : IEquatable<LicenseDetail> // IEquatable<LicenseDetail> Used to compare Licenses to see if they are identical.
    {

        [DisplayName("License Key")]
        [RegularExpression(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", ErrorMessage="Invalid License Type")]  // For validation of the GUID
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]  // Key and Auto Identified id
        public Guid LicenseKey { get; set; }

        public string ProductName { get; set; }
        
        [UIHint("License Capability")]
        [DisplayName("License Subscription Level")]
        public LicenseSubscription.LicenseType LicenseLevel { get; set; }
        
        [Required(ErrorMessage = "Required"), Range(0, 100, ErrorMessage = "Must be Valid number of Licenses")]
        [DisplayName("Number of Allocated Licenses")]
        public int NumberOfLicensesAllowed { get; set; }
        public int NumberOfLicensesClaimed { get; set; }

        public Owner OwnerDetails { get; set; }
        public ServerDetails Server { get; set; }

        public DateTime? LicenseValidUntilCurrent { get; set; }
               
        public static DateTime ValidUntilDate()
        {
            try
            {
                DateTime validUntil;
                DateTime.TryParse(ConfigProperty.GetValue("LiSense.validUntilDate"), out validUntil);
                return validUntil;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public LicenseDetail()
        {
            OwnerDetails = new Owner();
            Server = new ServerDetails();
        }
        
        public bool Equals(LicenseDetail otherLicense)
        {
            if (otherLicense == null)
            {
                return false;
            }

            // TODO: Granularly compare all relevant items and return True if all match equally, or false if any Fail
            // Currently matches license to the web response license to ensure it is valid.
            return this.ProductName.Equals(otherLicense.ProductName) &&
                this.LicenseKey.Equals(otherLicense.LicenseKey) &&
                this.LicenseLevel.Equals(otherLicense.LicenseLevel) &&
                this.Server.Server.HostName.Equals(otherLicense.Server.Server.HostName);
                //this.Server.MotherBoard.SerialNumber.Equals(otherLicense.Server.MotherBoard.SerialNumber) &&
                //this.Server.MotherBoard.ManufacturerName.Equals(otherLicense.Server.MotherBoard.ManufacturerName);
        }

        //public bool Equals(T otherLicense)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
