# README #

This README outlines some of the basic details that are needed for utilizing the functionality of the LiSense library.


### How do I get set up? ###

* The library can be used in your C# code by including it through adding a reference to the .dll file.
* After adding a reference to it, you will add 'using LiSense;' to your code's using statements
* The app.config or web.config will need to have the following node added to it in the <configuration> node (which specifies the licensing server that it needs to reach out to)

```
#!xml

  <appSettings>
    <add key="LiSense.validUntilDate" value="1/1/2016" />
    <add key="LiSense.endPointUrl" value="http://localhost/LiSense/api/Validate" />
  </appSettings>


```
* The code can then be used in the startup process (or which ever process you choose).

--------------------------------------------------------------------------------------

[Validate the License file against the LiSense server]

```
#!c#

using LiSense;


License lic = new License(License.Load("testLicense.lic").LicenseDetails);  // Load the local .lic file for License details.
var validateLicense = lic.Validate();


if (validateLicense && !lic.Errors.Any())
{
    Console.WriteLine("You are now in the application...");
    Console.ReadLine();
}

Console.WriteLine("Exiting application, License does not match...");
Environment.Exit(1);


```


[Create a New License file]

```
#!c#


License xLiSense = new License(new LicenseDetail()
{
    // Pull the following information from local .lic (license) file
    // If NO LicenseKey is present, then redirect user to input GUID   Product Key to register their software  (during registration, input all the database records required to authenticate the license.
    LicenseKey = Guid.Parse("18c860d0-bf66-e511-a9a2-000c29068e33"),
    ProductName = "Test123",
    LicenseLevel = LicenseSubscription.LicenseType.Full,
    Server = new LiSense.ServerDetails()
    {
        Server = new LiSense.ServerDetails.ServerDetail()
        {
            HostName = "HOST123"
        },
        MotherBoard = new LiSense.ServerDetails.MotherBoardDetail()
        {
            ManufacturerName = "Test123MotherBoard",
            SerialNumber = "Test1234567890"
        }
    }
});

xLiSense.Create(xLiSense, "testLicense.lic");

```

